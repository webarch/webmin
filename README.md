# Webarchitects Webmin Ansible Role

An Ansible role to configure the Webmin apt repo, install Webmin and set the Webmin root password, [Webmin](https://webmin.com/) on Debian and Ubuntu.

## Usage

The webmin root password is written to `/root/.webmin.root.passwd`, if this file doesn't exist it will be created and the root password will be reset based on the value in this file, so to reset the password delete `/root/.webmin.root.passwd` and run this role.

Once Webin is installed you can login at `https://webmin.example.org:10000/` using the username `root` and the password from `/root/.webmin.root.passwd`.

## Copyright

Copyright 2024 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
